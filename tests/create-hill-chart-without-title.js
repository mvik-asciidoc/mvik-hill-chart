'use strict'

const {task, drawHillChart} = require('../src/hill-chart.js')
const fs = require('fs')
const {createHill} = require("../src/hill-chart");

const hill = createHill({title: ''})

const tasks = [
    task('getting started', 0.1),
    task('almost there', 0.64, 'red'),
    task('soon done', 0.9, 'steelblue', 5)
]

console.log('create png')
const png = drawHillChart({hill, tasks})
fs.writeFileSync('./hill-chart-no-title.png', png)

console.log('done', +new Date())
