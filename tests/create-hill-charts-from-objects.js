'use strict'

const {task, drawHillChart} = require('../src/hill-chart.js')
const fs = require('fs')

const tasks = [
    task('getting started', 0.1),
    task('getting started on \na big task', 0.21, 'yellow'),
    task('almost there', 0.64, 'red'),
    task('woopeee!!', 0.85, 'cyan', 17),
    task('soon done', 0.9, 'steelblue', 5)
]

console.log('create png, good')
const pngGood = drawHillChart({tasks, config: {patternQuality: 'good'}})
fs.writeFileSync('./hill-chart.png', pngGood)

console.log('create png, best')
const pngBest = drawHillChart({tasks, config: {patternQuality: 'best'}})
fs.writeFileSync('./hill-chart-best.png', pngBest)

console.log('create svg')
const svg = drawHillChart({tasks}, 'svg')
fs.writeFileSync('./hill-chart.svg', svg)

console.log('done', +new Date())
