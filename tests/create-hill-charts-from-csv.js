'use strict'

const {createTasksFromCsv, drawHillChart, createHill} = require('../src/hill-chart')
const fs = require('fs')

const hill = createHill({title: 'Tasks'})
const tasks = createTasksFromCsv(`
getting started;0.1;purple;15
almost there;0.65;dark-pink
also almost there;0.65;light-yellow
soon done;0.9;steelblue;5
not getting anywhere;0.65;light-green
`)

console.log('create png')
fs.writeFileSync('./csv-hill-chart.png', drawHillChart({hill, tasks}))

console.log('create svg')
fs.writeFileSync('./csv-hill-chart.svg', drawHillChart({hill, tasks}, 'svg'))

console.log('done', +new Date())
