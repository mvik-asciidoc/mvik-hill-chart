const fs = require('fs')
const { createCanvas, loadImage } = require('canvas')

const width = 1200
const height = 630

const canvas = createCanvas(width, height)
const ctx = canvas.getContext('2d')

ctx.fillStyle = '#000'
ctx.fillRect(0, 0, width, height)

ctx.font = 'bold 70pt Menlo'
ctx.textAlign = 'center'
ctx.textBaseline = 'top'
ctx.fillStyle = '#3574d4'

const text = 'Mikael Vik!'

const textWidth = ctx.measureText(text).width
ctx.fillRect(600 - textWidth / 2 - 10, 170 - 5, textWidth + 20, 120)
ctx.fillStyle = '#fff'
ctx.fillText(text, 600, 170)

ctx.fillStyle = '#fff'
ctx.font = 'bold 30pt Menlo'
ctx.fillText('mvik.gitlab.io', 600, 530)

/*
loadImage('./logo.png').then(image => {
	ctx.drawImage(image, 340, 515, 70, 70)
})
*/
const buffer = canvas.toBuffer('image/png')
fs.writeFileSync('./test.png', buffer)

console.log('done')
