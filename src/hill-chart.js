'use strict'

const assert = require('assert')
const {createCanvas} = require("canvas")
const {parse: parseCsvWithPapa} = require('papaparse')
const {colors} = require('./tachyons-colors')

const textColor = '#2a363b'
const defaultDotRadius = 7

const point = (x, y) => ({x, y})

const createHill = ({title = 'Chart Title', backgroundColor = '#fff', hillChartSize: size = point(700, 250), hillChartOffset: offset = point(50, 100)} = {}) => {
    if (!title) {
        // noinspection JSSuspiciousNameCombination
        offset.y = offset.x
    }
    const start = point(offset.x, offset.y + size.y)
    const middle = point(offset.x + size.x / 2, offset.y)
    const end = point(offset.x + size.x, offset.y + size.y)

    return {
        title,
        backgroundColor,
        hillChartSize: size,
        hillChartOffset: offset,
        windowSize: point(size.x + offset.x * 2, size.y + offset.y + offset.x),
        points: {
            start,
            cpl1: point(start.x + size.x / 5, start.y + size.y / 50),
            cpl2: point(middle.x - size.x / 5, middle.y - size.y / 50),
            middle,
            cpr1: point(middle.x + size.x / 5, middle.y - size.y / 50),
            cpr2: point(end.x - size.x / 5, end.y + size.y / 50),
            end
        }
    }
}

const drawBackground = (ctx, {windowSize, backgroundColor}) => {
    ctx.fillStyle = backgroundColor
    ctx.fillRect(0, 0, windowSize.x, windowSize.y)
}

const drawTitle = (ctx, {title, hillChartSize: size, points: {middle}}) => {
    ctx.font = `${size.x / 30}pt sans serif`
    ctx.textAlign = 'center'
    ctx.textBaseline = 'top'
    ctx.fillStyle = textColor
    ctx.fillText(title, middle.x, 20)
}

const drawTaskText = (ctx, p, text, align, baseline) => {
    ctx.font = `10pt sans serif`
    ctx.textAlign = align
    ctx.textBaseline = 'middle'
    ctx.fillStyle = textColor
    ctx.fillText(text, p.x, p.y)
}

const drawVerticalLine = (ctx, {hillChartOffset: offset, hillChartSize: size, points: {middle}}) => {
    ctx.beginPath()
    for (let i = offset.y - 10; i < offset.y + size.y + 30; i += 30) {
        ctx.lineWidth = 0.3
        ctx.strokeStyle = '#96BBCB'
        ctx.moveTo(middle.x, i)
        ctx.lineTo(middle.x, i + 10)
        ctx.stroke()
    }
}

const drawHill = (ctx, {points: ps}) => {
    ctx.moveTo(ps.start.x, ps.start.y)
    ctx.bezierCurveTo(ps.cpl1.x, ps.cpl1.y, ps.cpl2.x, ps.cpl2.y, ps.middle.x, ps.middle.y)
    ctx.bezierCurveTo(ps.cpr1.x, ps.cpr1.y, ps.cpr2.x, ps.cpr2.y, ps.end.x, ps.end.y)
    ctx.lineWidth = 1
    ctx.strokeStyle = textColor
    ctx.stroke()
}

const drawDot = (ctx, p, fill = 'green', radius = 10) => {
    ctx.beginPath()
    ctx.arc(p.x, p.y, radius, 0, 2 * Math.PI)
    ctx.fillStyle = colors[fill] || fill
    ctx.fill()
    ctx.lineWidth = 0.7
    ctx.strokeStyle = textColor
    ctx.stroke()
}

const calculateBezier = (t, start, cp1, cp2, end) => {
    return Math.pow(1 - t, 3) * start +
        3 * t * Math.pow(1 - t, 2) * cp1 +
        3 * t * t * (1 - t) * cp2 +
        t * t * t * end
}

const calculateBezierPoint = (t, s, cp1, cp2, e) => ({
    x: calculateBezier(t, s.x, cp1.x, cp2.x, e.x),
    y: calculateBezier(t, s.y, cp1.y, cp2.y, e.y)
})

const task = (description, progress = 0.5, fill = 'lightgreen', radius = defaultDotRadius) =>
    ({description, progress, fill, radius})


const createTasksFromCsv = (csv) => {
    const results = parseCsvWithPapa(csv, {delimiter: ';', skipEmptyLines: true, dynamicTyping: true})
    return results.data.map(result => {
        const [description, progress, fill, radius] = result
        return task(description, progress, fill, radius)
    })
}

const drawTask = (ctx, {points: ps}, {description, progress, fill, radius}, stackIndex = 0) => {

    assert(0 <= progress && progress <= 1, `invalid progress value: ${progress}`)

    const p = progress <= 0.5 ?
        calculateBezierPoint(progress * 2, ps.start, ps.cpl1, ps.cpl2, ps.middle) :
        calculateBezierPoint(progress * 2 - 1, ps.middle, ps.cpr1, ps.cpr2, ps.end)

    if (stackIndex) {
        const stackOffset = radius * stackIndex * 1.9
        p.y = p.y - stackOffset
    }
    drawDot(ctx, p, fill, radius)

    const textOffset = radius + 10 - defaultDotRadius
    if (progress < 0.2) {
        drawTaskText(ctx, point(p.x + textOffset, p.y), description, 'left', 'top')
    } else if (progress < 0.5) {
        drawTaskText(ctx, point(p.x - textOffset, p.y), description, 'right', 'bottom')
    } else if (progress < 0.8) {
        drawTaskText(ctx, point(p.x + textOffset, p.y), description, 'left', 'bottom')
    } else {
        drawTaskText(ctx, point(p.x - textOffset, p.y), description, 'right', 'top')
    }
}

const drawHillChart = ({tasks = [], hill = createHill(), config = {}}, type = 'image/png') => {
    const canvas = createCanvas(hill.windowSize.x, hill.windowSize.y, type)
    const ctx = canvas.getContext('2d')
    ctx.patternQuality = config.patternQuality || 'best'

    drawBackground(ctx, hill)
    drawTitle(ctx, hill)
    drawVerticalLine(ctx, hill)
    drawHill(ctx, hill)

    let previous = undefined
    let stackIndex = 0
    tasks
        .sort((a, b) => a.progress - b.progress)
        .forEach(t => {
            if (previous?.progress !== t.progress) {
                stackIndex = 0
            } else {
                stackIndex++
            }
            drawTask(ctx, hill, t, stackIndex);
            previous = t
        })
    return canvas.toBuffer()
}

module.exports = {
    point,
    task,
    createTasksFromCsv,
    createHill,
    drawHillChart
}
