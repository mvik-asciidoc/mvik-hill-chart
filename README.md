# @mvik/hill-chart

Create [hill charts](https://basecamp.com/shapeup/3.4-chapter-13#work-is-like-a-hill) using JavaScript.

Supports `png` and `svg`.

## Installation 

`npm install @mvik/hill-chart`

## Plain JS

```js
const {task, createHill, drawHillChart} = require('@mvik/hill-chart')
const fs = require('fs')

const hill = createHill({title: 'New Title'})

const tasks = [
    task('getting started', 0.1),
    task('almost there', 0.64, 'red'),
    task('soon done', 0.9, 'steelblue', 5)
]

const svg = drawHillChart({hill, tasks}, 'svg')
fs.writeFileSync('./hill-chart.svg', svg)
```

## Tasks from CSV

The csv is parsed using [Papa Parse](https://www.papaparse.com/).

```js
const {createTasksFromCsv, drawHillChart} = require('../src/hill-chart')
const fs = require('fs')

const hill = createHill({title: 'Tasks'})
const tasks = createTasksFromCsv(`
getting started;0.1
almost there;0.64;red
soon done;0.9;steelblue;5
`)

const png = drawHillChart({hill, tasks})
fs.writeFileSync('./csv-hill-chart.png', png)
```

## Result

![generated png](csv-hill-chart.png)
